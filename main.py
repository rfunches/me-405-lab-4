#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
@file main.py
@brief       Main method for temperature data collection.
@details     Allows the user to communicate with MCP9808 temperature sensor 
             using a I2C interface, as well as the built on core temperature reader
             in the Nucleo. This file initializes all variables relating to temperature
             sensing and then collects data at a fixed rate of one data point per minute.
             While data is being collected, it is simultaneously stored in a local CSV file on the Nucleo for
             data manipulation. Data collection occurs for 8 hours or a user input of "CTRL+C", whichever comes
             first.
             
             Link to Source Code: https://bitbucket.org/rfunches/me-405-lab-4/src/master/main.py
                 
@author      Ashley Humpal and Ryan Funchess
@copyright   License Info
@date        February 5, 2021

'''
import pyb
import utime
from array import array
#from pyb import UART
from pyb import I2C
from mcp9808 import tempsensor


#Initialization of all readers

## Variable representing ADCALL object
adc=pyb.ADCAll(12,0x70000) #create ADCAll object for temperature reading
## Variable initializing ADCall temperature
starter=adc.read_core_vref() #initialize ADCall for temp reading

#i2c
## Variable representing master device
i2cM = I2C(1, I2C.MASTER)             # create and init as a master

## Variable representing sensor device
sensor=tempsensor(i2cM,24) #declares tempsensor object, passes I2C object and sensor address


## Nucleo Temperature array data
NucleoTemp=array('f') #Nucleo temperature array to store values

## Sensor Temperature array data
SensorTemp=array('f') #Sensor temperature array to store values

print('About to take data')

## Counter for loop
counter=0 #counter to intialize while loop condition
with open ("tempdataa.csv","w") as a_file: #open CSV file

    while True: #While loop created
        try: #do the following    
            while(counter<=480): #loop for 480 minutes (or CTRL+C, whichever first)
## Intermediate variable representing nucleo temperature
                NuTemp=adc.read_core_temp() #Nucleo temperature from adc
                NucleoTemp.append(NuTemp) #append Nucleo temp to array
## Intermediate variable representing sensor temperature            
                STemp=sensor.celsius() #sensor temperature from object
                SensorTemp.append(STemp) #append sensor temperature to array
                
                a_file.write('{:},{:},{:}\r\n'.format(counter,NucleoTemp[counter],SensorTemp[counter])) #append to csv file
                print('{:},{:},{:}\r\n'.format(counter,NucleoTemp[counter],SensorTemp[counter]))
                
                utime.sleep(60) #sleep for 1 minute
                counter=counter+1 #increment counter
                
            if counter>480: #if longer than 480 minutes
                break 
    
        except KeyboardInterrupt: #if cntrl C then do following
            break
    

print ("The file has by now automatically been closed.")








# version without try and except 
# '''
# @file main.py

#  Link to Source Code: https://bitbucket.org/rfunches/me-405-lab-4/src/master/main.py

# doc: https://ahumpal.bitbucket.io/me405/main2_8py.html
# '''
# import pyb
# import utime
# from array import array
# #from pyb import UART
# from pyb import I2C
# from mcp9808 import tempsensor


# #Initialization of all readers

# #core temp reader
# adc=pyb.ADCAll(12,0x70000) #create ADCAll object for temperature reading
# starter=adc.read_core_vref() #initialize ADCall for temp reading

# #i2c
# i2cM = I2C(1, I2C.MASTER)             # create and init as a master

# sensor=tempsensor(i2cM,24)


# #collect data for 8 hours

# NucleoTemp=array('f') #Nucleo temperature array to store values
# SensorTemp=array('f') #Sensor temperature array to store values

# print('About to take data')
# counter=0 #counter to intialize while loop condition
# while(counter<=480): #loop for 480 minutes
#     utime.sleep(60) #sleep for 1 minute
#     NuTemp=adc.read_core_temp() #Nucleo temperature from adc
#     NucleoTemp.append(NuTemp) #append Nucleo temp to array
    
#     STemp=sensor.celsius() #sensor temperature from object
#     SensorTemp.append(STemp) #append sensor temperature to array
    
#     counter=counter+1 #increment counter
    

# #store data in a file


# with open ("R.txt","w") as a_file: #open CSV file
#     for n in range(len(NucleoTemp)): #go through all data points
#         a_file.write('{:},{:},{:}\r\n'.format(n,NucleoTemp[n],SensorTemp[n])) #append to csv file
#         print('{:},{:},{:}\r\n'.format(n,NucleoTemp[n],SensorTemp[n]))

# print ("The file has by now automatically been closed.") 

# #create I2C bus object, temp from Nucleo

# print(SensorTemp)
# print(NucleoTemp)



