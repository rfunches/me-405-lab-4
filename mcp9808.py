#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file mcp9808.py
@brief       Code that serves as a driver for the MCP9808 Temperature Sensor.
@details     Allows the user to communicate with MCP9808 temperature sensor 
             using its I2C interface. Includes methods to check if the sensor 
             is attatched to the correct bus address, and register celsius and fahrenheit
             temperature values. Takes I2C object and temperature sensor address
             as input parameters.
             
             Link to Source Code: https://bitbucket.org/rfunches/me-405-lab-4/src/master/mcp9808.py
                 
@author      Ashley Humpal and Ryan Funchess
@copyright   License Info
@date        February 5, 2021
"""


from pyb import I2C #import need modules
import utime

## New class representing the MCP9808 Temperature Sensor
class tempsensor:
    '''
    @brief      Class to represent MCP9808 Temperature Sensor
    @details    This class implements a check method to check if the sensor 
                is attatched to the correct bus address, and collects celsius and fahrenheit
                temperature values. 
    '''


      
    def __init__(self, I2Cobject, Sensoraddress):
        
   
        '''
        @brief Creates methods for interfacing with MCP9808 temperature sensor using its I2C interface.
        @param I2object  An object from class tempsensor representing master device and bus.
        @param Sensoraddress An object from class tempsensor representing the sensor device
        
        '''
## Variable representing I2C master device
        self.I2C=I2Cobject  #initializing parameters
## Variable representing sensor address
        self.Sensoraddress=Sensoraddress

        
    def check(self): 
        '''
        @brief     Verifies that the sensor is attached at the given bus address
        @details   Checks that the value in the manufacturer ID register is correct
    
        '''
## Variable representing state of connection from master to sensor
        TF=self.I2C.is_ready(self.SensorAddress) #I2 command to check against bus address
        return TF
        
        #Checks if an I2C device responds to the given address
        
        
 
        
    def celsius(self):
        '''
        @brief     Returns measured temperature in degrees Celsius
        @details   Converts raw temperature data from bytes to numerical value
        '''
        # return self.tim.counter()
## Variable representing temperature reading from sensor
        reading=self.I2C.mem_read(2, self.Sensoraddress, 5, timeout=500, addr_size=8)
        #Reading 2 bytes, address of sensor, register bit in dec, timeout in milliseconds to wait for the read, wdith of memory address

## Variable representing first byte
        byte0=reading[0]  #gets first byte binary total value 
## Variable representing second byte
        byte1=reading[1]
## Variable representing binary numbers of byte 1
        byt0=bin(byte0) #converts byte binary total to actaul binary values
## Variable representing binary numbers of byte 2
        byt1=bin(byte1)
       
        if float(byt0[5])==1: #if statement to check the sign of temperature reading
## Variable representing sign of temperature
            sign= -1 # if binary number is 1, then sign of temp is negative
        else:
            sign=1 #otherwise temp is positive
## Intermediate variable representing byte position 6 of byte1
        by0pos4=float(byt0[6])*(2**7) #converts binary number to float then multiplies that by temp conversion value for this specific byte position 
## Intermediate variable representing byte position 7 of byte1
        by0pos5=float(byt0[7])*(2**6) #converts binary number to float then multiplies that by temp conversion value for this specific byte position
## Intermediate variable representing byte position 8 of byte1
        by0pos6=float(byt0[8])*(2**5)#converts binary number to float then multiplies that by temp conversion value for this specific byte position
## Intermediate variable representing byte position 9 of byte1
        by0pos7=float(byt0[9])*(2**4)#converts binary number to float then multiplies that by temp conversion value for this specific byte position
        
        
## Intermediate variable representing byte position 2 of byte2       
        by1pos0=float(byt1[2])*(2**3) #converts binary number to float then multiplies that by temp conversion value for this specific byte position
## Intermediate variable representing byte position 3 of byte2
        by1pos1=float(byt1[3])*(2**2)#converts binary number to float then multiplies that by temp conversion value for this specific byte position
## Intermediate variable representing byte position 4 of byte2
        by1pos2=float(byt1[4])*(2**1)#converts binary number to float then multiplies that by temp conversion value for this specific byte position
## Intermediate variable representing byte position 5 of byte2
        by1pos3=float(byt1[5])*(2**0) #converts binary number to float then multiplies that by temp conversion value for this specific byte position
## Intermediate variable representing byte position 6 of byte2
        by1pos4=float(byt1[4])*(2**-1)#converts binary number to float then multiplies that by temp conversion value for this specific byte position
## Intermediate variable representing byte position 7 of byte2
        by1pos5=float(byt1[6])*(2**-2)#converts binary number to float then multiplies that by temp conversion value for this specific byte position
## Intermediate variable representing byte position 8 of byte2
        by1pos6=float(byt1[7])*(2**-3) #converts binary number to float then multiplies that by temp conversion value for this specific byte position
## Intermediate variable representing byte position 9 of byte2
        by1pos7=float(byt1[8])*(2**-4)#converts binary number to float then multiplies that by temp conversion value for this specific byte position

## Variable representing sensor temperature in degrees Celsius        
        temp= sign * (by0pos4 + by0pos5 + by0pos6 +by0pos7 +by1pos0
                      +by1pos1 + +by1pos2 + +by1pos3 + +by1pos4 
                      +by1pos5 +by1pos6 +by1pos7) #sums every byte position value and multiplies by sign
        return(temp) #return temp to referenced file

        
    def fahrenheight(self):
        '''
        @brief      Returns measured temperature in degrees Fahrenheight
        @details   Converts raw temperature data from bytes to numerical value
        '''        

        temp=self.celsius() #calls upon conversions in celsius method
## Variable representing Fahrenheight temperature value 
        tempF=temp*1.8+32 #converting from Celsius to Fahrenheight
        return tempF
        
if __name__=='__main__':
    i2cM = I2C(1, I2C.MASTER)             # create and init as a master
    sensor=tempsensor(i2cM,24)
    while True:
        utime.sleep(1) #sleep for 1 minute
        print(str(sensor.celsius))
        
 
    